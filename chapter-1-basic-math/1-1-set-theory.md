# Chapter 1: Basic Math

## 1.1 Set Theory

1. A _set_ is an **unordered** collection of objects.
> **Note**: $$\lbrace1,2,3\rbrace=\lbrace3,1,2\rbrace$$.

2. If $$S$$ contains exactly $$n$$ **distinct** elements where $$n$$ is a nonnegative integer, $$S$$ is a **finite set** and its **cardinality** $$\left|S\right|=n$$.
> **Note**: $$\left|\lbrace1,1,2,3,3,3\rbrace\right|=\left|\lbrace1,2,3\rbrace\right|=3$$.

3. $$x$$ is an **element** of $$A$$<br>$$\iff x\in A$$.

4. $$A$$ is a **subset** of $$B$$<br>$$\iff A\subseteq B$$<br>$$\iff\forall x(x\in A\rightarrow x\in B)$$.

5. $$A$$ is a **proper subset** of $$B$$<br>$$\iff A\subset B$$<br>$$\iff A\subseteq B\text{ and }A\ne B$$<br>$$\iff\forall x(x\in A\rightarrow x\in B)\land\exists x(x\in B\land x\notin A)$$.

6. $$A$$ and $$B$$ are **equal** sets<br>$$\iff A=B$$<br>$$\iff A\subseteq B\text{ and }B\subseteq A$$<br>$$\iff\forall x(x\in A\leftrightarrow x\in B)$$.

7. $$\emptyset=\lbrace\ \rbrace$$ is the **empty set** or **null set**.

8. A set with one element is called a **singleton set**.

> **Note**: $$\left|\emptyset\right|=0\text{, but }\left|\lbrace\emptyset\rbrace\right|=1$$.

---

**Theorem**: For every set $$S$$, $$\text{(i) }\emptyset\subseteq S$$ and $$\text{(ii) }S\subseteq S$$

**Proof**:

\(i\) To show that $$\emptyset\subseteq S$$, we must show that $$\forall x(x\in\emptyset\rightarrow x\in S)$$ is true.
Since $$\emptyset$$ contains no elements, $$x\in\emptyset$$ is always false.
It follows that $$x\in\emptyset\rightarrow x\in S$$ is always true, since a conditional statement with a false hypothesis is true.
Therefore, $$\forall x(x\in\emptyset\rightarrow x\in S)$$ is true.

Note that this is an example of a _vacuous proof_[^1].

\(ii\) To show that $$S\subseteq S$$, we must show that $$\forall x(x\in S\rightarrow x\in S)$$ is true.
Because $$p\rightarrow p\equiv\lnot p\lor p\equiv\mathbf T$$, it follows that $$x\in S\rightarrow x\in S$$ is always true.
Therefore, $$\forall x(x\in S\rightarrow x\in S)$$ is true.

---

Ways to describe a set:

* **roster method**, e.g., $$\lbrace1,3,5,7,\ldots\rbrace$$
* **set builder** notation, e.g., $$\lbrace2k+1\mid k\in\Bbb N\rbrace$$, or equivalently, $$\lbrace k\in\Bbb N\mid k\text{ is odd}\rbrace$$
* **intervals** of real numbers: when $$a$$ and $$b$$ are real numbers,
  * $$[a, b]=\lbrace x\mid a\le x\le b\rbrace$$
  * $$[a, b)=\lbrace x\mid a\le x\lt b\rbrace$$
  * $$(a, b]=\lbrace x\mid a\lt x\le b\rbrace$$
  * $$(a, b)=\lbrace x\mid a\lt x\lt b\rbrace$$.

---

#### Number systems

$$\Bbb N=\lbrace0,1,2,3,\ldots\rbrace$$, the set of **natural numbers**[^2]

$$\Bbb Z=\lbrace\ldots,-2,-1,0,1,2,\ldots\rbrace$$, the set of **integers**

$$\Bbb Z^+=\lbrace 1,2,3,\ldots\rbrace$$, the set of **positive integers**

$$\Bbb Q=\lbrace p/q\mid p\in\Bbb Z,q\in\Bbb Z,\text{and }q\ne0\rbrace$$, the set of **rational numbers**

$$\Bbb R=(-\infty,\infty)$$, the set of **real numbers**

$$\overline{\Bbb Q}=\lbrace x\mid x\in\Bbb R\land x\notin\Bbb Q\rbrace$$, the set of **irrational numbers**

$$\Bbb R^+=(0,\infty)$$, the set of **positive real numbers**

$$\Bbb C=\lbrace a+bi\mid a\in\Bbb R,b\in\Bbb R,\text{and }i^2=-1\rbrace$$, the set of **complex numbers**.
> **Note**: $$\Bbb N\subset\Bbb Z\subset\Bbb Q\subset\Bbb R\subset\Bbb C$$

---

**Example**: What is the cardinality of $$\lbrace5,5,\lbrace5\rbrace,\lbrace5,5\rbrace,\lbrace5,\lbrace5,5\rbrace,\lbrace5,5,\lbrace5\rbrace,5,5\rbrace\rbrace\rbrace$$ ?

$$\left|\lbrace5,5,\lbrace5\rbrace,\lbrace5,5\rbrace,\lbrace5,\lbrace5,5\rbrace,\lbrace5,5,\lbrace5\rbrace,5,5\rbrace\rbrace\rbrace\right|=\left|\lbrace5,\lbrace5\rbrace,\lbrace5,\lbrace5\rbrace,\lbrace5,\lbrace5\rbrace\rbrace\rbrace\rbrace\right|=3$$.

---

**Example**: \(99成大\) Which of the following are true?

> 1. $$\emptyset\subseteq\lbrace\emptyset\rbrace$$
> 2. $$\emptyset\subseteq\emptyset$$
> 3. $$\emptyset\subset\lbrace\emptyset\rbrace$$
> 4. $$\emptyset\subset\emptyset$$
> 5. $$\emptyset\in\lbrace\emptyset\rbrace$$

1. True, since $$\emptyset\subseteq S\text{ for any set }S$$.
2. True, since $$S\subseteq S\text{ for any set }S$$.
3. True, since $$\emptyset\subseteq\lbrace\emptyset\rbrace\text{ and }\emptyset\ne\lbrace\emptyset\rbrace$$.
4. False, since $$\emptyset=\emptyset$$.
5. True, since $$\emptyset$$ is an element of $$\lbrace\emptyset\rbrace$$.

---

**Example**: \(98台大\) Which of the following are true?

> 1. $$\emptyset\in\emptyset$$
> 2. $$\lbrace\emptyset,\lbrace\emptyset\rbrace\rbrace\in\lbrace\emptyset,\lbrace\emptyset\rbrace,\lbrace\emptyset,\lbrace\emptyset\rbrace\rbrace\rbrace$$
> 3. $$\lbrace\emptyset,\lbrace\emptyset\rbrace\rbrace\subset\lbrace \emptyset,\lbrace\emptyset\rbrace,\lbrace\emptyset,\lbrace\emptyset\rbrace\rbrace\rbrace$$
> 4. $$\lbrace a,\lbrace b,c\rbrace\rbrace=\lbrace\lbrace c,b\rbrace,a\rbrace$$

1. False, since $$\emptyset$$ contains no elements.
2. True, since $$\lbrace\emptyset,\lbrace\emptyset\rbrace\rbrace$$ is an element of $$\lbrace\emptyset,\lbrace\emptyset\rbrace,\lbrace\emptyset,\lbrace\emptyset\rbrace\rbrace\rbrace$$.
3. True, since $$\lbrace\emptyset,\lbrace\emptyset\rbrace\rbrace\subseteq\lbrace\emptyset,\lbrace\emptyset\rbrace,\lbrace\emptyset,\lbrace\emptyset \rbrace \rbrace \rbrace$$ and $$\lbrace \emptyset,\lbrace\emptyset \rbrace\rbrace\ne\lbrace\emptyset,\lbrace\emptyset\rbrace,\lbrace\emptyset,\lbrace\emptyset\rbrace\rbrace\rbrace$$.
4. True, since they have the same elements \(order doesn't matter\).

---

#### Set operations

1. The **union** of $$A$$ and $$B$$<br>$$=A\cup B$$<br>$$=\lbrace x\mid x\in A\lor x\in B\rbrace$$.

2. The **intersection** of $$A$$ and $$B$$<br>$$=A\cap B$$<br>$$=\lbrace x\mid x\in A\land x\in B\rbrace$$.

3. The **difference** of $$A$$ and $$B$$<br>$$=A\setminus B$$<br>$$=A-B$$<br>$$=\lbrace x\mid x\in A\land x\notin B\rbrace$$.

4. The **complement** of $$A$$ **with respect to the universal set** $$U$$<br>$$=\overline A$$<br>$$=\lbrace x\in U\mid x\notin A\rbrace$$
> Note: The difference of $$A$$ and $$B=$$ the complement of $$B$$ with respect to $$A$$.

5. The **symmetric difference** of $$A$$ and $$B$$<br>$$=A\bigtriangleup B$$<br>$$=A\ominus B$$<br>$$=A\oplus B$$<br>$$=(A\cup B)\setminus(A\cap B)$$<br>$$=(A\setminus B)\cup(B\setminus A)$$<br>$$=\lbrace x\mid x\in A\oplus x\in B\rbrace$$

---

#### Set Identities

| Identity | | Name |
| :--- | :--- | :--- |
| $$A\cap U=A$$ | $$A\cup\emptyset=A$$ | Identity laws |
| $$A\cup U=U$$ | $$A\cap\emptyset=\emptyset$$ | Domination laws |
| $$A\cup A=A$$ | $$A\cap A = A$$ | Idempotent laws |
| $$\overline{(\overline A)}=A$$ | | Complementation law |
| $$A\cup B=B\cup A$$ | $$A\cap B=B\cap A$$ | Commutative laws |
| $$A\cup(B\cup C)=(A\cup B)\cup C$$ | $$A\cap(B\cap C)=(A\cap B)\cap C$$ | Associative laws |
| $$A\cup(B\cap C)=(A\cup B)\cap(A\cup C)$$ | $$A\cap(B\cup C)=(A\cap B)\cup(A\cap C)$$ | Distributive laws |
| $$\overline{A\cap B}=\overline A\cup\overline B$$ | $$\overline{A\cup B}=\overline A\cap\overline B$$ | De Morgan's laws |
| $$A\cup(A\cap B)=A$$ | $$A\cap(A\cup B)=A$$ | Absorption laws |
| $$A\cup\overline A=U$$ | $$A\cap\overline A=\emptyset$$ | Complement laws |


[^1]: A _vacuous proof_ happens when the hypothesis of an implication is always false.

[^2]: Mathematicians disagree whether 0 is a natural number. We consider it quite natural.
